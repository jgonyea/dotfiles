# Dotfiles

This dotfiles management is cribbed heavily from [Andrew Burgess](https://github.com/andrew8088/dotfiles) dotfiles repository.


[Here's a description of how my dotfiles work](https://shaky.sh/simple-dotfiles/).

## Install

```
git clone <this-repo>
cd <this-repo>
./install/bootstrap.sh
```

## Local ZSH Config

If there's customization you want ZSH to load on startup that is specific to
this machine (stuff you don't want to commit into the repo), create `~/.env.sh`
and put it in there. It will be loaded near the top of `.zshrc`.


## External tools required
- [Difftastic](https://difftastic.wilfred.me.uk/installation.html)
- zsh-syntax-highlighting
```bash
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
```