# ALIASES ---------------------------------------------------------------------
alias killyubi='sudo /etc/init.d/pcscd stop'
alias ll="clear && ls -la"
alias lzd='docker run --rm -it -v /var/run/docker.sock:/var/run/docker.sock -v ~/.config/lazydocker:/.config/jesseduffield/lazydocker lazyteam/lazydocker'
alias genmac="python3 -c 'import random; print(\"BC:24:%02x:%02x:%02x:%02x\" % (random.randint(0,255), random.randint(0,255), random.randint(0,255), random.randint(0,255)))'"


# Alias for systems with vim and not vi
if which vim >/dev/null; then
    alias vi="vim"
else
    alias vim="vi"
fi

note() {
    NOTE_PATH="$HOME/Nextcloud/Notes"
    echo "date: $(date)" >> $NOTE_PATH/cli_notes.txt
    echo "$@" >> $NOTE_PATH/cli_notes.txt
    echo "" >> $NOTE_PATH/cli_notes.txt
}

bigfile() {
    if [ $# -eq 0 ]; then
        du -ah . | sort -rh | head -15
    else
        du -ah $1 | sort -rh | head -15
    fi
}
